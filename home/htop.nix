{ config, options, ... }:

{
  config = {
    programs.htop = {
      enable = true;
      settings = let
        htop = config.lib.htop;
      in {
        fields = let
          f = htop.fields;
          f' = builtins.mapAttrs (n: v: toString v) htop.fields;
          substitute = subs: map (x: subs.${toString x} or x);
        in substitute {
          ${f'.TIME} = f.STARTTIME;
        } htop.defaultFields;
        hide_userland_threads = true;
        show_program_path = false;
        screen_tabs = true;
        show_cpu_frequency = true;
        tree_view = true;
      } // (htop.leftMeters [
        (htop.bar "LeftCPUs")
        (htop.bar "Memory")
        (htop.bar "Swap")
        (htop.text "ZFSARC")
      ]) // (htop.rightMeters [
        (htop.bar "RightCPUs")
        (htop.text "Tasks")
        (htop.text "LoadAverage")
        (htop.text "Uptime")
      ]);
    };
  };
}
