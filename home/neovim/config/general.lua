-- General settings
vim.o.mouse = "a"
vim.o.wildmode = "longest:full,full"
vim.o.splitright = true
vim.o.splitbelow = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.undofile = true
vim.g.netrw_list_hide = [[\(^\|\s\s\)\zs\.\S\+]]

if vim.uv.getuid() == 0 then
	vim.o.backup = false
	vim.o.swapfile = false
	vim.o.undofile = false
end

-- Editor look
vim.o.wrap = true
vim.o.breakindent = true
vim.o.scrolloff = 5
vim.o.number = true
vim.o.cursorline = true
vim.o.colorcolumn = "+1"
vim.o.list = true
vim.o.listchars = "tab:  ,trail:•,precedes:…,extends:…"
vim.o.showmode = false

-- Color scheme
vim.o.termguicolors = true
vim.o.background = "light"
vim.cmd.colorscheme("selenized_bw")

-- Formatting rules
--vim.o.cino = ":0(su0Wsm1"
vim.api.nvim_create_autocmd("FileType", {
	pattern = {"text", "markdown"},
	callback = function ()
		vim.bo.textwidth = 80
	end,
})
vim.api.nvim_create_autocmd("FileType", {
	pattern = "python",
	callback = function ()
		vim.bo.textwidth = 79
	end,
})
