{ lib, pkgs, ... }:

{
  config = {
    local = {
      shell.aliases = let
        args = lib.escapeShellArgs [
          "--classify"

          "--binary"
          "--group" "--smart-group"
          "--git"

          # Workaround for https://github.com/eza-community/eza/issues/682
          "--color-scale-mode=fixed"
        ];
      in {
        eza = "eza ${args}";
      };
      shell.abbreviations = {
        ls = "eza";
        lsa = "eza -a";
        ll = "eza -l";
        lla = "eza -la";
        tree = "eza -lT";
      };
    };

    home = {
      packages = [ pkgs.eza ];

      sessionVariables = {
        EZA_COLORS = let
          pairs = val: keys: map (k: "${k}=${val}") keys;
        in lib.concatStringsSep ":" (
          [
            "reset"
            "oc=0"
            "xa=0"
            "lc=0"
            "lm=38;5;15;1"
            "xx=38;5;8;1"
            "da=0"
            "in=0"
            "hd=38;5;7;1"
            "lp=0"
            "cc=38;5;1"
            "bO=38;5;1;7"
            "sp=4"
            "mp=38;5;4;48;5;0;1"
          ]
          # User permissions
          ++ (pairs "1" [ "ur" "uw" "ux" "ue" ])
          # Other permission bits
          ++ (pairs "0" [ "gr" "gw" "gx" "tr" "tw" "tx" ])
          ++ (pairs "38;5;1;7" [ "su" "sf" ])
          # Sizes
          ++ (pairs "0" [ "sn" "sb" "bl" ])
          # Device IDs
          ++ (pairs "0" [ "df" "ds" ])
          # Ownership
          ++ (pairs "1" [ "uu" "gu" ])
          ++ (pairs "38;5;1;1" [ "uR" "gR" ])
          ++ (pairs "0" [ "un" "gn" ])
          # Git
          ++ [
            "ga=38;5;2"
            "gd=38;5;1"
            "gi=0"
            "gc=38;5;3"
          ]
          ++ (pairs "38;5;5" [ "gm" "gv" "gt" "Gd" ])
          ++ (pairs "0" [ "Gm" "Go" "Gc" ])
          # SELinux
          ++ (pairs "0" [ "Sn" "Su" "Sr" "St" "Sl" ])
        );
      };
    };
  };
}
