{ config, flakeInputs, pkgs', ... }:

let
  home-manager-patched = pkgs'.applyPatches {
    name = "home-manager-patched";
    src = flakeInputs.home-manager;
    patches = [
      ./0001-librespot-init-module.patch
      ./0002-htop.patch
    ];
  };
in {
  imports = [
    (home-manager-patched + /nixos)
  ];

  config = {
    home-manager = {
      useUserPackages = true;
      useGlobalPkgs = true;
      extraSpecialArgs = {
        inherit flakeInputs;
        systemProfile = config.systemProfile;
      };
    };

    home-manager.users.systemOwner = import ../../home;
  };
}
