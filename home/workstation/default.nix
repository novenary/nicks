{ lib, pkgs, systemProfile, ... }:

{
  imports = lib.optionals systemProfile.isWorkstation (import ./module-list.nix);

  config = {
    home = {
      packages = [
        pkgs.gimp
        pkgs.helvum
        pkgs.libreoffice
        pkgs.obs-studio
        pkgs.pavucontrol
        pkgs.super-slicer
        pkgs.syncplay
        (pkgs.xdg-utils.overrideAttrs (old: {
          patches = old.patches or [] ++ [
            ./xdg/0001-xdg-open-open-terminal-applications-in-TERMINAL.patch
          ];
        }))

        (pkgs.signal-desktop.overrideAttrs (old: {
          postInstall = old.postInstall or "" + ''
            sed -i $out/lib/Signal/resources/app.asar \
              -e '/^  const windowOptions = {$/{n;s/^    show: false,$/    show: true, /}'
          '';
        }))
      ];

      sessionVariables = {
        NIXOS_OZONE_WL = 1;

        # Stop Wine from installing file associations
        WINEDLLOVERRIDES = "winemenubuilder.exe=d";
      };
    };
  };
}
