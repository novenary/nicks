set -l prompt

for i in (seq 0 7)
	# Not an array because fish indexes from 1
	set -f pr_fg_$i \e\[38\;5\;{$i}m
end
set -l pr_nofg \e\[39m
set -l pr_bg \e\[48\;5\;0m
set -l pr_nobg \e\[49m
set -l pr_bold \e\[1m
set -l pr_nobold \e\[22m
set -l pr_reset \e\[0m

# Start of prompt (FinalTerm/foot)
set -a prompt \e\]133\;A\e\\

set -a prompt $pr_bg

# Nested shell indicator
test $SHLVL -gt 1
and set -a prompt $pr_fg_1 $SHLVL:

set -q IN_NIX_SHELL
and set -a prompt $pr_fg_4 $name $pr_bold λ $pr_nobold

if fish_is_root_user
	set -a prompt $pr_fg_1
else
	set -a prompt $pr_fg_2
end
set -a prompt $USER

if set -q SSH_TTY
	set -a prompt $pr_fg_5
else
	set -a prompt $pr_fg_4
end
set -a prompt @ (prompt_hostname)

set -a prompt $pr_nofg :
set -l realhome (string escape --style=regex -- ~)
# Replace $HOME with ~, split path into components
set -l cwd (string replace -r '^'"$realhome"'($|/)' '~$1' $PWD | string split /)
# Make the final component green
set cwd[-1] $pr_fg_2$cwd[-1]
# Make slashes red
set -a prompt (string join $pr_fg_1/$pr_nofg $cwd) $pr_nofg

set -a prompt (gitprompt-rs)

set -a prompt $pr_reset \n $pr_bold "\$ " $pr_reset

echo -ns $prompt
