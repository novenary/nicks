{ pkgs, ... }:

{
  config = {
    home.packages = [ pkgs.swayimg ];
    xdg.configFile."swayimg/config".source =
      (pkgs.formats.ini {}).generate "swayimgrc" {
        general.size = "fullscreen";
        viewer.window = "#000000FF";
        gallery.window = "#000000FF";
      };
  };
}
