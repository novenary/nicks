local palette

if vim.o.background == 'dark' then
	palette = {
		base00 = '#181818', -- bg_0
		base01 = '#252525', -- bg_1
		base02 = '#3b3b3b', -- bg_2
		base03 = '#777777', -- dim_0
		base04 = '#777777', -- dim_0
		base05 = '#b9b9b9', -- fg_0
		base06 = '#dedede', -- fg_1
		base07 = '#dedede', -- fg_1
		base08 = '#ed4a46', -- red
		base09 = '#e67f43', -- orange
		base0A = '#dbb32d', -- yellow
		base0B = '#70b433', -- green
		base0C = '#3fc5b7', -- cyan
		base0D = '#368aeb', -- blue
		base0E = '#eb6eb7', -- magenta
		base0F = '#a580e2', -- violet
	}
end

if vim.o.background == 'light' then
	palette = {
		base00 = '#ffffff', -- bg_0
		base01 = '#ebebeb', -- bg_1
		base02 = '#cdcdcd', -- bg_2
		base03 = '#878787', -- dim_0
		base04 = '#878787', -- dim_0
		base05 = '#474747', -- fg_0
		base06 = '#282828', -- fg_1
		base07 = '#282828', -- fg_1
		base08 = '#d6000c', -- red
		base09 = '#d04a00', -- orange
		base0A = '#c49700', -- yellow
		base0B = '#1d9700', -- green
		base0C = '#00ad9c', -- cyan
		base0D = '#0064e4', -- blue
		base0E = '#dd0f9d', -- magenta
		base0F = '#7f51d6', -- violet
	}
end

if palette then
	require('mini.base16').setup({ palette = palette })

	-- lualine
	vim.api.nvim_set_hl(0, "winbar_diag_warning", {
		fg = palette.base0A, bg = "NONE", reverse = true, bold = true,
	})

	-- indent-blankline
	vim.api.nvim_set_hl(0, "IblIndent", {fg = palette.base02})
	vim.api.nvim_set_hl(0, "IblScope", {fg = palette.base03})

	-- mini.diff
	vim.api.nvim_set_hl(0, "MiniDiffSignAdd", {fg = palette.base0B, reverse = true})
	vim.api.nvim_set_hl(0, "MiniDiffSignDelete", {fg = palette.base08, reverse = true})
	vim.api.nvim_set_hl(0, "MiniDiffSignChange", {fg = palette.base0E, reverse = true})

	vim.g.colors_name = 'selenized_bw'
end
