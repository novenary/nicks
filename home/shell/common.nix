{ lib, pkgs, ... }:

{
  options = {
    local.shell = {
      aliases = lib.mkOption {
        type = let t = lib.types; in t.attrsOf t.str;
        default = {};
        description = "Common shell aliases";
      };
      abbreviations = lib.mkOption {
        type = let t = lib.types; in t.attrsOf t.str;
        default = {};
        description = "Common shell abbreviations";
      };
    };
  };

  config = {
    local.shell = {
      abbreviations = {
        cp = "cp -r";
        rsync = "rsync -rPh --info=PROGRESS2";
        pgrep = "pgrep -l";
        verynice = "schedtool -B -n 15 -e ionice -c3";
      };
    };

    programs.atuin = {
      enable = true;
      package = pkgs.atuin.overrideAttrs (old: {
        doCheck = false; # Tests take too long to build
        cargoBuildNoDefaultFeatures = true;
        cargoBuildFeatures = [
          "client"
        ];
        patches = old.patches or [] ++ [
          (pkgs.fetchpatch {
            url = "https://raw.githubusercontent.com/Mic92/dotfiles/refs/heads/main/home-manager/pkgs/atuin/0001-make-atuin-on-zfs-fast-again.patch";
            hash = "sha256-35D727tx9lRwxs7XhuGmr9g4IbDfH8FDxQ/VSMgKidM=";
          })
        ];
      });
      flags = [
        "--disable-up-arrow"
      ];
      settings = {
        update_check = false;
        enter_accept = false;
        show_preview = true;
      };
    };
  };
}
