local mappings = {
	"*",
	"#",
	"g*",
	"g#",
	"z*",
	"gz*",
	"z#",
	"gz#",
}
for _, m in ipairs(mappings) do
	vim.keymap.set({"n", "v"}, m, "<Plug>(asterisk-" .. m .. ")")
end
vim.g["asterisk#keeppos"] = 1
