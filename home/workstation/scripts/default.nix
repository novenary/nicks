{ config, pkgs, ... }:

{
  config = {
    home.packages = [
      (pkgs.writeShellApplication {
        name = "clipshot";
        text = ''
          slurp_args=(-c ${config.miscAttrs.colors.hex.violet})
        '' + builtins.readFile ./clipshot;
        runtimeInputs = [
          pkgs.jq
          pkgs.grim
          pkgs.slurp
          pkgs.wl-clipboard
        ];
      })

      (pkgs.writers.writeFishBin "fzf_run" {
        makeWrapperArgs = [
          "--prefix" "PATH" ":" "${pkgs.fzf}/bin"
        ];
      } ./fzf_run.fish)

      (pkgs.writeShellScriptBin "colortest.sh" (builtins.readFile ./colortest.sh))
    ];
  };
}
