{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ../../common

    ./jellyfin.nix
    ./libvirt.nix
    ./metalfan.nix
    ./ratbag.nix
    ./transmission.nix
    ./ups.nix
    ./zrepl.nix
  ];

  config = {
    boot.kernelPackages = pkgs.linuxPackages_6_12;
    boot.supportedFilesystems = [ "zfs" ];
    boot.kernelParams = [ "zfs.zfs_arc_max=${builtins.toString (24 * 1024 * 1024 * 1024)}" ];
    boot.zfs.devNodes = "/dev/";

    services.zfs.trim.enable = true;
    services.zfs.autoScrub = {
      enable = true;
      interval = "Sun, 05:00";
    };

    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;

    networking.hostName = "Akatsuki"; # Define your hostname.
    networking.hostId = "39745438";

    time.timeZone = "Asia/Jerusalem";

    i18n = {
      defaultLocale = "en_US.UTF-8";
      extraLocaleSettings = {
        LC_TIME = "en_DK.UTF-8";
      };
    };
    console = {
      keyMap = "colemak/mod-dh-matrix-us";
    };

    systemProfile = {
      isWorkstation = true;
    };

    networking.firewall = {
      allowedTCPPorts = [
        # Sonos/SoCo
        # TODO move Sonos volume control to a system module?
        1400
      ];
      allowedUDPPorts = [
      ];
    };

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "24.11"; # Did you read the comment?
  };
}
