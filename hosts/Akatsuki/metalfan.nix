{ ... }:

{
  config = {
    boot.kernelModules = [ "nct6775" ];

    services.metalfan = {
      enable = true;

      config = {
        interval = 1;

        hwmons = {
          cpu = {
            path = "/sys/bus/pci/drivers/k10temp/0000:*/hwmon/hwmon*";
            probes = [ "temp1" ];
          };
          superio = {
            path = "/sys/devices/platform/nct6775.656/hwmon/hwmon*";
          };
          gpu = {
            path = "/sys/bus/pci/drivers/amdgpu/0000:*/hwmon/hwmon*";
            probes = [ "temp1" ];
          };
        };

        fangroups = [
          # CPU
          {
            probe = "cpu/temp1";
            temp_min = 60;
            temp_max = 105;
            speed = [ "superio/fan2" ];
            pwm = [ "superio/pwm2" ];
            pwm_stop = 93;
            pwm_min = 93;
            pwm_max = 255;
          }

          # GPU
          {
            probe = "gpu/temp1";
            temp_stop = 60;
            temp_min = 50;
            temp_max = 90;
            speed = [ "gpu/fan1" ];
            pwm = [ "gpu/pwm1" ];
            pwm_stop = 35;
            pwm_min = 31;
            pwm_max = 100;
          }

          # Case fans
          {
            probe = "gpu/temp1";
            temp_min = 62;
            temp_max = 90;
            speed = [ "superio/fan3" "superio/fan4" ];
            speed_min = 310;
            pwm = [ "superio/pwm3" "superio/pwm4" ];
            pwm_stop = 153;
            pwm_start = 80;
            pwm_min = 90;
            pwm_max = 204;

            # The noise is annoying under mild loads
            pwm_fixed = 90;
          }

          # Chipset fan
          {
            pwm = [ "superio/pwm5" ];
            pwm_fixed = 102;
          }

          # PSU fan
          {
            speed = [ "superio/fan1" ];
            speed_min = 400;
            pwm = [ "superio/pwm1" ];
            pwm_fixed = 90;
          }
        ];
      };
    };
  };
}
