{ pkgs, ... }:

{
  config = {
    programs.neovim = {
      enable = true;
      package = pkgs.neovim-unwrapped.overrideAttrs (old: {
        patches = old.patches or [] ++ [
          # runtime(rust): use shiftwidth() in indent script
          (pkgs.fetchpatch {
            url = "https://github.com/neovim/neovim/commit/c49bcde9271a5bc22decbdf1a173207e9932fbf3.patch";
            hash = "sha256-DUpgMBvtMNHsLu4xm/iRUXshDjHwPRnXXdUsXgsHD7M=";
          })
        ];
      });
      defaultEditor = true;
      extraLuaConfig = ''
        if vim.env.NVIM_IMPURE_CONFIG then
          vim.opt.runtimepath:prepend("~/nicks/home/neovim/impure")
        end
        require("vim.site.config")
      '';

      plugins = let
        p = pkgs.vimPlugins;
        pu = pkgs.unstable.vimPlugins;
      in [
        # Visuals
        p.lualine-nvim
        p.nvim-treesitter-context
        p.indent-blankline-nvim

        # Search
        p.leap-nvim
        p.flit-nvim
        p.vim-asterisk
        (p.nvim-window-picker.overrideAttrs (old: {
          patches = old.patches or [] ++ [
            ./nvim-window-picker.patch
          ];
        }))

        # Misc
        p.vim-repeat # required by leap, flit
        p.vim-sleuth
        p.undotree
        p.vim-scriptease

        # Language support
        p.vim-nix # Partially included with (n)vim, but missing indentation rules

        # nvim extensions
        p.nvim-treesitter.withAllGrammars
        p.plenary-nvim # required by telescope
        p.telescope-nvim

        # Code completion
        p.nvim-lspconfig
        pu.blink-cmp

        p.mini-nvim
      ];
    };

    xdg.configFile = {
      "nvim/lua/vim/site/config".source = ./config;
    };

    xdg.dataFile = {
      "nvim/site".source = ./site;
    };

    local = {
      shell.abbreviations = {
        v = "nvim";
        sv = "sudo --preserve-env=HOME nvim";
      };
    };

    home = {
      sessionVariables = {
        DIFFPROG = "nvim -d";
        MANPAGER = "nvim +Man!";
      };
    };
  };
}
