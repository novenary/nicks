{ config, lib, pkgs, ... }:

let
  cfg = config.programs.fish;
  shellCommon = config.local.shell;
in {
  config = {
    assertions = [
      {
        assertion = cfg.shellAliases == {};
        message = "fish aliases are broken, use functions or local.shell.aliases.";
      }
    ];

    home.packages = [ pkgs.gitprompt-rs ];

    programs.fish = {
      enable = true;

      interactiveShellInit = ''
        functions -e fish_greeting
        functions -e fish_command_not_found

        function _local_postexec --on-event fish_postexec
          set -l last_status $status
          set -l last_duration $CMD_DURATION

          test $last_status -ne 0
          and echo "$(set_color -r red) $last_status $(set_color normal)"

          test $last_duration -gt 5000; and begin
            set -l msecs (math -s0 "($CMD_DURATION) % 1000")
            set -l secs (math -s0 "($CMD_DURATION / (1000)) % 60")
            set -l mins (math -s0 "($CMD_DURATION / (1000*60)) % 60")
            set -l hours (math -s0 "($CMD_DURATION / (1000*60*60)) % 24")
            set -l days (math -s0 "($CMD_DURATION / (1000*60*60*24))")

            set -l time
            test $days -gt 0; and set -a time {$days}d
            test $hours -gt 0; and set -a time {$hours}h
            test $mins -gt 0; and set -a time {$mins}m
            set -a time (printf "%d.%03ds" $secs $msecs)

            echo $argv[1]: $time
          end

          # BELL to draw attention to the terminal
          echo -n \a
        end

        set -g fzf_args -- -0 --reverse --height=~50%
      '';

      functions = {
        sudo = let
          aliasesStr = lib.concatStringsSep "\n" (
            lib.mapAttrsToList (name: body: ''
              case ${name}
                set argv ${body} $argv[2..-1]
                test $argv[1] = ${name}; and break
              '') shellCommon.aliases
          );
        in ''
          while true
            switch $argv[1]
            ${aliasesStr}
            case '*'
              break
            end
          end
          command sudo $argv
        '';

        fish_user_key_bindings = ''
          bind \cz 'fg 2>/dev/null; commandline -f repaint'
          bind ,ff _fzf_files

          set -g fish_sequence_key_delay_ms 1000
        '';

        fish_prompt = builtins.readFile ./prompt.fish;

        _sudo_abbr_expander = let
          abbrsStr = lib.concatStringsSep "\n" (
            lib.mapAttrsToList (name: body: ''
              case ${name}
                echo ${lib.escapeShellArg body}
              '') shellCommon.abbreviations
          );
        in ''
          set -l toks (commandline -opc)
          test "$toks[-1]" = "$argv[1]"; and set -e toks[-1]
          test "$toks[-1]" = 'sudo'; or return 1

          switch $argv[1]
            ${abbrsStr}
          end
        '';

        _fzf_files = builtins.readFile ./fzf_files.fish;
      };

      shellAbbrs = let
        sudoAbbrs = lib.mapAttrs' (name: body: {
          name = "_sudo_${name}";
          value = {
            position = "anywhere";
            regex = name;
            function = "_sudo_abbr_expander";
          };
        }) shellCommon.abbreviations;
      in shellCommon.abbreviations // sudoAbbrs;
    };
    programs.man.generateCaches = false;

    xdg.configFile = lib.mapAttrs' (name: words: {
      name = "fish/functions/${name}.fish";
      value.text = ''
        status --is-interactive; and begin
          set -l words ${words}

          if test $words[1] != ${name}
            complete -e ${name}
            function ${name} --wraps ${lib.escapeShellArg words}
              ${words} $argv
            end
          else
            if builtin -q ${name}
              # FIXME: The builtin "builtin" *fails to parse* if the builtin does not exist
              printf 'Tried to alias %s (a builtin) to itself, but this is not implemented!\n' ${name} >&2
              return 1
            else
              function ${name}
                command ${words} $argv
              end
            end
          end
        end
      '';
    }) shellCommon.aliases // {
      "fish/completions/man.fish".source = ./man.fish;
    };
  };
}
