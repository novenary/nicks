{ config, lib, pkgs, ... }:

{
  imports = [
    ./wlsunset.nix
  ];

  config = {
    home.file.".session".source = pkgs.writeShellScript ".session" ''
      exec systemd-cat --identifier=sway sway
    '';

    home.packages = [
      (pkgs.runCommand "font-awesome_6_patched" {} (let
        fontPath = "share/fonts/opentype";
      in ''
        mkdir -p "$out/${fontPath}"
        ln -s "${pkgs.font-awesome_6}/${fontPath}/Font Awesome 6 Free-Solid-900.otf" \
          "$out/${fontPath}"
      ''))
    ];

    wayland.windowManager.sway = {
      enable = true;
      wrapperFeatures.gtk = true;

      config = let
        cfg = config.wayland.windowManager.sway.config;
        color_theme = config.miscAttrs.colors.html;
        cursorCfg = config.home.pointerCursor;
      in {
        fonts = {
          names = [ "IBM Plex Sans" "Font Awesome 6 Free Solid" ];
          size = 10.0;
        };

        focus = {
          followMouse = "no";
          newWindow = "urgent";
          wrapping = "yes";
        };
        seat."*" = {
          xcursor_theme = "${cursorCfg.name} ${toString cursorCfg.size}";
          hide_cursor = "2000";
        };

        input = {
          "type:keyboard" = {
            xkb_layout = "us(colemak_dh_ortho),il(phonetic)";
            xkb_options = "lv3:ralt_alt";
          };
          "type:pointer" = {
            accel_profile = "flat";
          };
          "type:touchpad" = {
            click_method = "clickfinger";
            natural_scroll = "enabled";
            tap = "enabled";
            dwt = "disabled";
          };
          "1386:890:Wacom_One_by_Wacom_S_Pen" = {
            map_from_region = ".0x.1 1x1";
          };
          "1390:269:ELECOM_TrackBall_Mouse_HUGE_TrackBall" = {
            scroll_method = "on_button_down";
            scroll_button = "279";
            middle_emulation = "enabled";
          };
          # DualShock 4 touch pad
          "1356:2508:Wireless_Controller_Touchpad".events = "disabled";
        };

        window = {
          titlebar = false;
          border = 4;
          commands = [
            /*
            {
              command = "fullscreen disable";
              criteria = {
                app_id = "^org.telegram.desktop$";
                title = "^Media viewer";
              };
            }
            */
            {
              command = "move scratchpad";
              criteria = {
                app_id = "^firefox";
                title = " — Sharing Indicator$";
              };
            }
            {
              command = "move scratchpad";
              criteria = {
                title = " is sharing (your screen|a window)\.$";
              };
            }
          ];
        };

        floating = {
          titlebar = true;
          border = cfg.window.border;
          criteria = [
            { app_id = "modalterm"; }
          ];
        };

        colors = let
          colorSet = border: text: {
            border = border;
            background = border;
            text = text;
            indicator = border;
            childBorder = border;
          };
        in {
          focused = colorSet color_theme.blue color_theme.bg_1;
          focusedInactive = colorSet color_theme.fg_0 color_theme.bg_0;
          unfocused = colorSet color_theme.bg_2 color_theme.dim_0;
          urgent = colorSet color_theme.red color_theme.bg_1;
        };

        output = {
          "*" = {
            background = "${color_theme.win2k_blue} solid_color";
            subpixel = "none";
          };
          "Samsung Electric Company U32J59x HTPK700098" = {
            scale = "2";
          };
        };

        bars = [
          {
            fonts = cfg.fonts;
            position = "bottom";
            extraConfig = ''
              status_edge_padding 0
              status_padding 0
              icon_theme ${config.gtk.iconTheme.name}
            '';
            statusCommand = (pkgs.python3.pkgs.callPackage ./status {}) + /bin/swaystatus;

            colors = let
              workspace = background: text: {
                border = background;
                background = background;
                text = text;
              };
            in {
              separator = color_theme.dim_0;
              background = color_theme.bg_1;
              statusline = color_theme.fg_0;
              focusedWorkspace = workspace color_theme.blue color_theme.bg_1;
              activeWorkspace = workspace color_theme.fg_0 color_theme.bg_0;
              inactiveWorkspace = workspace color_theme.bg_2 color_theme.fg_0;
              urgentWorkspace = workspace color_theme.red color_theme.bg_1;
            };
          }
        ];

        defaultWorkspace = "workspace number 1";
        modifier = "Mod4";
        bindkeysToCode = true;
        keybindings = let
          mapKeys = keys: names: mod: action: builtins.listToAttrs (
            lib.zipListsWith (key: name: {
              name = "${mod}+${key}";
              value = "${action} ${name}";
            }) keys names);

          dirKeys = [ "m" "n" "e" "i" ];
          dirNames = [ "left" "down" "up" "right" ];
          mapDirs = mapKeys (dirKeys ++ dirNames) (dirNames ++ dirNames);

          workspaceKeys = (builtins.genList (n: toString (lib.mod (n + 1) 10)) 10);
          workspaceNames = (builtins.genList (n: "number ${toString (n + 1)}") 10);
          mapWorkspaces = mapKeys workspaceKeys workspaceNames;

          volumeKeys = [ "XF86AudioRaiseVolume" "XF86AudioLowerVolume" "XF86AudioMute" ];
          volumeActions = map (a: "exec ${pkgs.pulseaudio}/bin/pactl ${a}") (let
            PA_VOLUME_NORM = 65536;
            steps = 16;
            step = PA_VOLUME_NORM / steps;
            step' = toString step;
          in [
            "set-sink-volume @DEFAULT_SINK@ +${step'}"
            "set-sink-volume @DEFAULT_SINK@ -${step'}"
            "set-sink-mute @DEFAULT_SINK@ toggle"
          ]);
          sonosCommand = a: "exec ${pkgs.netcat}/bin/nc -NU /run/user/$UID/sonos_volume <<< ${a}";
          sonosActions = map sonosCommand [ "+" "-" "x" "l" ];

          appMenu = config.miscAttrs.terminal {
            command = "fzf_run";
            modal = true;
          };
        in lib.mkMerge [
          (mapDirs "${cfg.modifier}" "focus")
          (mapDirs "${cfg.modifier}+Shift" "move")
          (mapWorkspaces "${cfg.modifier}" "workspace")
          (mapWorkspaces "${cfg.modifier}+Shift" "move container to workspace")
          (mapDirs "${cfg.modifier}+Ctrl" "move workspace to output")
          (mapKeys volumeKeys volumeActions "" "")
          (mapKeys (volumeKeys ++ [ "XF86AudioPlay" ]) sonosActions "Shift" "")
          {
            "${cfg.modifier}+q" = "kill";

            "${cfg.modifier}+space" = "focus mode_toggle";
            "${cfg.modifier}+a" = "focus parent";
            "${cfg.modifier}+d" = "focus child";

            "${cfg.modifier}+x" = "split toggle";
            "${cfg.modifier}+Shift+x" = "split none";
            "${cfg.modifier}+s" = "layout toggle split";
            "${cfg.modifier}+w" = "layout toggle stacking tabbed";

            "${cfg.modifier}+f" = "fullscreen toggle";
            "${cfg.modifier}+Shift+space" = "floating toggle";

            "${cfg.modifier}+Escape" = "workspace back_and_forth";

            "${cfg.modifier}+Shift+c" = "reload";
            "${cfg.modifier}+Shift+q" = "exit";

            "${cfg.modifier}+r" = "exec ${appMenu}";
            "${cfg.modifier}+Return" = "exec ${config.miscAttrs.terminal {}}";
            "${cfg.modifier}+Shift+Return" = "exec ${config.miscAttrs.terminal {
              command = "ranger";
            }}";

            "${cfg.modifier}+p" = "exec clipshot sel";
            "${cfg.modifier}+Shift+p" = "exec clipshot active";
            "${cfg.modifier}+Ctrl+p" = "exec clipshot full";

            "XF86MonBrightnessUp" = "exec xbacklight -perceived -inc 5";
            "XF86MonBrightnessDown" = "exec xbacklight -perceived -dec 5";

            "${cfg.modifier}+Semicolon" = "input type:keyboard xkb_switch_layout next";
          }
        ];
        modes = {};
      };

      extraConfig = ''
        titlebar_padding 5 1
        hide_edge_borders --i3 none
      '';
    };
  };
}
