local blink = require("blink.cmp")

local metatable = {}
metatable.__index = function(t, key)
	if not rawget(t, key) then
		rawset(t, key, setmetatable({}, metatable))
	end
	return rawget(t, key)
end
local cfg = setmetatable({}, metatable)

cfg.keymap = {
	["<C-y>"] = { "accept", "fallback" },
}

cfg.completion.keyword.range = "full"
cfg.completion.list.selection = {
	preselect = false,
	auto_insert = true,
}
cfg.completion.accept.auto_brackets.enabled = false
cfg.completion.menu.draw.columns = {
	{ "label", "label_description", gap = 1 }, { "kind" }
}
cfg.completion.documentation = {
	auto_show = true,
	auto_show_delay_ms = 0,
}

cfg.signature.enabled = true

cfg.sources.providers.buffer.opts = {
	-- Buffer completion from all open buffers
	get_bufnrs = function()
		return vim.tbl_filter(function(bufnr)
			return vim.bo[bufnr].buftype == ""
		end, vim.api.nvim_list_bufs())
	end,
}
cfg.sources.providers.path.opts = {
	trailing_slash = false,
}

blink.setup(cfg)

local lspconfig = require("lspconfig")
local capabilities = blink.get_lsp_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = false
lspconfig.pylsp.setup { capabilities = capabilities }
lspconfig.rust_analyzer.setup {
	capabilities = capabilities,
	completion = { capable = { snippets = "add_parenthesis" } },
}
vim.lsp.handlers["textDocument/publishDiagnostics"] = nil
