{ flakeInputs, lib, ... }:

let
  inherit (lib) filterAttrs mapAttrs';
  inputs = {
    inherit
      (flakeInputs)
      nixpkgs
      nixpkgs-unstable
      ;
  };
  flakes = filterAttrs (name: value: value ? outputs) inputs;
in {
  config = {
    # Inhibit default behavior
    nixpkgs.flake.source = lib.mkForce null;

    nix.registry = builtins.mapAttrs (name: v: { flake = v; }) flakes;

    environment.etc = mapAttrs' (name: value: {
      name = "nix/inputs/${name}";
      value = {
        source = value.outPath;
      };
    }) inputs;
    nix.nixPath = [
      "/etc/nix/inputs"
    ];
  };
}
