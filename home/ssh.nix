{ ... }:

{
  config = {
    programs.ssh = {
      enable = true;
      forwardAgent = true;
      serverAliveInterval = 300;
      matchBlocks = {
        "taiga.lan" = {
          port = 2222;
        };
      };
    };
  };
}
