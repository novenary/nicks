{ lib, pkgs, ... }:

{
  config = let
    keybindsPath = "TelegramDesktop/tdata/shortcuts-custom.json";
  in {
    home.packages = [ pkgs.tdesktop ];

    xdg.dataFile."${keybindsPath}".text = builtins.toJSON (lib.mapAttrsToList (k: v: {
      command = v;
      keys = k;
    }) {
      "ctrl+q" = null;
      "ctrl+w" = null;
      "ctrl+n" = "next_chat";
      "ctrl+p" = "previous_chat";
    });

    # Workaround for tdesktop not quitting upon closing the window
    # Fucking weird.
    # https://github.com/telegramdesktop/tdesktop/issues/28016#issuecomment-2150984606
    dconf.settings."org/gnome/desktop/wm/preferences" = {
      button-layout = ":";
    };
  };
}
