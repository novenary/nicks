{ pkgs, ... }:

{
  config = {
    home.packages = [ pkgs.udiskie ];
    services.udiskie = {
      enable = true;
      tray = "never";
    };
  };
}
