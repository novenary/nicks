vim.keymap.set("n", "<F9>", "<cmd>UndotreeToggle<CR>")
vim.g.undotree_SetFocusWhenToggle = 1
vim.g.undotree_ShortIndicators = 1
vim.g.undotree_HighlightChangedText = 0
