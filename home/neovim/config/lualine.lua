local function status_filename()
	local ret = "%f%m" -- filename, modified flag
	-- If buftype is not empty, then the buffer isn't a real file
	if vim.o.buftype == "" then
		-- nothing
	elseif vim.o.readonly then
		if vim.fn.filereadable(vim.fn.bufname("%")) == 0 then
			ret = ret .. "[noperm]"
		else
			ret = ret .. "[RO]"
		end
	end
	return ret
end

require("lualine").setup {
	options = {
		icons_enabled = false,
		section_separators = "",
		component_separators = "|",
		disabled_filetypes = {
			statusline = {"undotree"},
		},
	},
	sections = {
		lualine_a = {"mode"},
		lualine_b = {
			"%c%V", -- column number
		},
		lualine_c = {status_filename},
		lualine_x = {
			"filetype",
			-- Equivalent to airline's ffenc part
			"%{&enc}%{&bomb ? '[BOM]' : ''}%{&eol ? '' : '[!EOL]'}[%{&ff}]"
		},
		lualine_y = {},
		lualine_z = {},
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {status_filename},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {},
	},
	winbar = {
		lualine_a = {
			{
				"whitespace",
				color = "winbar_diag_warning",
				checks = {
					long_line = false,
				},
			},
		},
	},
	tabline = {
		lualine_a = {
			{
				"tabs",
				max_length = function()
					return vim.o.columns - 3
				end,
				mode = 2,
			},
		},
	},
}
