require("nvim-treesitter.configs").setup {
	auto_install = false,
	highlight = {
		enable = true,
	},
}

require("treesitter-context").setup {
	max_lines = 5,
	multiline_threshold = 1,
}
