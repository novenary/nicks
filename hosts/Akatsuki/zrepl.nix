{ config, lib, pkgs, ... }:

{
  config = {
    boot.zfs.extraPools = [ "index" ];
    # The import service retries for 60 seconds,
    # this should be plenty so don't pull in systemd-udevd-settle
    systemd.services."zfs-import-index".requires = lib.mkForce [];

    services.zrepl = {
      enable = true;
      settings = {
        jobs = [
          {
            name = "snapjob";
            type = "snap";
            filesystems = {
              "akatsuki_root/enc/home" = true;
              "akatsuki_root/enc/root" = true;
            };
            snapshotting = {
              type = "periodic";
              interval = "15m";
              prefix = "zrepl_";
            };
            pruning = {
              keep = [
                {
                  type = "grid";
                  grid = "1x1h(keep=all) | 24x1h | 14x1d";
                  regex = "^zrepl_.*";
                }
                {
                  type = "regex";
                  negate = true;
                  regex = "^zrepl_.*";
                }
              ];
            };
          }

          {
            type = "push";
            name = "push_to_index";
            connect = {
              type = "local";
              listener_name = "index_sink";
              client_identity = config.networking.hostName;
            };
            filesystems = {
              "akatsuki_root/enc/home" = true;
              "akatsuki_root/enc/root" = true;
            };
            send = {
              encrypted = true;
            };
            replication = {
              protection = {
                initial = "guarantee_resumability";
                incremental = "guarantee_resumability";
              };
            };
            snapshotting.type = "manual";
            pruning = {
              keep_sender = [
                {
                  type = "regex";
                  regex = ".*";
                }
              ];
              keep_receiver = [
                {
                  type = "grid";
                  grid = "1x1h(keep=all) | 24x1h | 360x1d";
                  regex = "^zrepl_.*";
                }
                {
                  type = "regex";
                  negate = true;
                  regex = "^zrepl_.*";
                }
              ];
            };
          }

          {
            type = "sink";
            name = "index_sink";
            root_fs = "index/zrepl";
            serve = {
              type = "local";
              listener_name = "index_sink";
            };
            recv = {
              placeholder = {
                encryption = "off";
              };
            };
          }
        ];
      };
    };

    systemd.services.zrepl-timer = {
      description = "Trigger zrepl replication";
      startAt = "*:0";
      serviceConfig = {
        Type = "oneshot";
        ExecStart = [
          "${pkgs.zrepl}/bin/zrepl signal wakeup push_to_index"
        ];
      };
    };
  };
}
