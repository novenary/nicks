{
  description = "Nick's Operating System (novie's config)";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/stable.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      flake = false;
    };
    firefox-nightly = {
      url = "github:nix-community/flake-firefox-nightly";
      flake = false;
    };
    mozilla = {
      url = "github:mozilla/nixpkgs-mozilla";
      flake = false;
    };
    gitprompt-rs = {
      url = "github:9ary/gitprompt-rs";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rqoob = {
      url = "github:redolution/rqoob";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, ... }@inputs: {
    nixosConfigurations = {
      "Akatsuki" = let
        pkgsConfig = {
          system = "x86_64-linux";
          config = {
            allowUnfree = true;
          };
        };
        pkgs = import nixpkgs (pkgsConfig // {
          overlays = [
            inputs.lix-module.overlays.default
            (final: prev: {
              unstable = pkgsUnstable;
            })
            (import ./overlays/sway.nix)
            inputs.gitprompt-rs.overlays.default
          ];
        });
        pkgsUnstable = import inputs.nixpkgs-unstable (pkgsConfig // {
        });
      in nixpkgs.lib.nixosSystem {
        inherit pkgs;
        specialArgs = {
          flakeInputs = inputs;
          pkgs' = pkgs;
        };
        modules = [
          ({config, ...}: {
            config.assertions = [
              {
                assertion = config.nixpkgs.overlays == [];
                message = "config.nixpkgs.overlays is slow, apply overlays during instantiation";
              }
            ];
          })

          ./hosts/Akatsuki/configuration.nix
          inputs.rqoob.nixosModules.default
        ];
      };
    };
  };
}
