{ config, lib, pkgs, ... }:

let
  shimName = "bashim";
  bashShim = pkgs.runCommand shimName {
    passthru = {
      shellPath = "/bin/${shimName}";
    };
  } ''
    mkdir -p "$out/bin"
    ln -s "${pkgs.bashInteractive}/bin/bash" "$out/bin/${shimName}"
  '';
in {
  config = {
    security.pam.sshAgentAuth.enable = true;

    users = {
      mutableUsers = false;
    };

    users.groups.systemOwner = {
      name = "novenary";
      gid = 1000;
    };
    users.users.systemOwner = {
      name = "novenary";
      isNormalUser = true;
      uid = 1000;
      group = "novenary";
      shell = bashShim;
      extraGroups = [ "wheel" "dialout" ];
      openssh.authorizedKeys.keys = [
        "ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzODQAAABhBG6H4lV15NkxRokSqykOD9zcGWiRLbQBpfCnQg3w1tnVwIl7sjmHMxYsgFsqe3K9EgyVUnBvK+1A/uK3KHw3LlsFi4jyLm+Q5tYCfMZrCPVASSnogHePAa9smkv1anBVhw== YubiKey #18386453 PIV Slot 9a"
        "ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzODQAAABhBFg87KD6iMJEIwqFyekR7JzqUrIotlmq0htgTFut8cBkeOXAPklfwnfjyox9W6SvDwLQtSBoQMQXGsWQ7fwFStZsTZkFiAUrtwYsRnk5bifBaz9ZlR/i/bMh5TY28mcrfw== YubiKey #19775926 PIV Slot 9a"
        "ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzODQAAABhBNAjssImUpp9brV8H5UHKVWvacg4X3xJ27d5LZX6PjO+RS9QEYG0txNCCj/EmXqr5E06NKNSL2/SxMUPHdkpifAlh0n6c8mxdFz8vBNbXQetMSUy6fGthgrT5cZLkLLE9A== YubiKey #19775916 PIV Slot 9a"
      ];
    };

    users.users.root = {
      openssh.authorizedKeys = config.users.users.systemOwner.openssh.authorizedKeys;
    };
  };
}
