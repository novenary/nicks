{ pkgs, ... }:

{
  home.pointerCursor = {
    package = pkgs.gnome-themes-extra;
    name = "Adwaita";
    size = 24;
    gtk.enable = true;
    x11.enable = true;
  };
}
