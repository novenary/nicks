vim.g.mapleader = ","

-- Ease fine movement on alt layouts
vim.o.whichwrap = ""
vim.keymap.set({"n", "v", "o"}, "<S-Space>", "<Left>")

vim.keymap.set("n", "<Leader>w", "<C-w>p")
vim.keymap.set("n", "<C-t>", "<cmd>tabnew<CR>")
vim.keymap.set("n", "<C-Tab>", "<cmd>tabnext<CR>")
vim.keymap.set("n", "<C-S-Tab>", "<cmd>tabprevious<CR>")

vim.keymap.set("n", "<Esc>", "<cmd>nohlsearch<CR>")

-- Trim trailing whitespace
vim.keymap.set("n", "<F5>", [[<cmd>let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>]])
