require("telescope").setup {
	defaults = {
		borderchars = {"─", "│", "─", "│", "┌", "┐", "┘", "└"},
		cache_picker = {
			num_pickers = 20,
		},
	}
}

local ts_builtin = require("telescope.builtin")
vim.keymap.set("n", "<Leader>ff", ts_builtin.find_files, {})
vim.keymap.set("n", "<Leader>fg", ts_builtin.live_grep, {})
vim.keymap.set("n", "<Leader>a", function()
	ts_builtin.grep_string({word_match = "-w"})
end, {})
vim.keymap.set("v", "<Leader>a", ts_builtin.grep_string, {})
vim.keymap.set("n", "<Leader>fb", ts_builtin.buffers, {})
vim.keymap.set("n", "<Leader>fh", ts_builtin.help_tags, {})
vim.keymap.set("n", "<Leader>t", ts_builtin.resume, {})
vim.keymap.set("n", "<Leader>fp", ts_builtin.pickers, {})
