{ config, lib, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  config = {
    boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
    boot.initrd.kernelModules = [ ];
    boot.kernelModules = [ "kvm-amd" ];
    boot.extraModulePackages = [ ];
    boot.zfs.requestEncryptionCredentials = [ "akatsuki_root/enc" ];
    boot.zfs.forceImportRoot = false;

    hardware.cpu.amd.ryzen-smu.enable = true;

    # Power saving
    boot.kernelParams = [ "amd_pstate=active" ];
    services.tlp = {
      enable = true;
      settings = {
        TLP_DEFAULT_MODE = "AC";
        TLP_PERSISTENT_DEFAULT = 1;

        CPU_SCALING_GOVERNOR_ON_AC = "powersave";
        CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
        CPU_ENERGY_PERF_POLICY_ON_AC = "balance_performance";
        CPU_ENERGY_PERF_POLICY_ON_BAT = "balance_performance";
        RUNTIME_PM_ON_AC = "auto";
        RUNTIME_PM_ON_BAT = "auto";

        DISK_APM_LEVEL_ON_AC = "keep";
        DISK_APM_LEVEL_ON_BAT = "keep";
        AHCI_RUNTIME_PM_ON_AC = "on";
        AHCI_RUNTIME_PM_ON_BAT = "on";
      };
    };

    boot.extraModprobeConfig = ''
      options amdgpu ppfeaturemask=0xffffffff
    '';

    fileSystems."/" =
      { device = "akatsuki_root/enc/root";
        fsType = "zfs";
      };

    fileSystems."/boot" =
      { device = "/dev/disk/by-uuid/B8CA-6D6E";
        fsType = "vfat";
      };

    fileSystems."/nix" =
      { device = "akatsuki_root/enc/nix";
        fsType = "zfs";
      };

    fileSystems."/var/tmp" =
      { device = "akatsuki_root/enc/tmp";
        fsType = "zfs";
        neededForBoot = true;
      };

    fileSystems."/home" =
      { device = "akatsuki_root/enc/home";
        fsType = "zfs";
      };

    fileSystems."/mnt/data" =
      { device = "/dev/disk/by-uuid/7aec7f58-44b4-4737-bd62-3dad9effa696";
        fsType = "btrfs";
        options = [ "nofail" "rw" "noatime" "compress-force=zstd" "subvol=data" ];
      };

    #fileSystems."/mnt/oldarch" =
    #  { device = "akatsuki_root/enc/oldarch/root";
    #    fsType = "zfs";
    #  };

    #fileSystems."/mnt/oldarch/home" =
    #  { device = "akatsuki_root/enc/oldarch/home";
    #    fsType = "zfs";
    #  };

    fileSystems."/mnt/games" =
      { device = "akatsuki_root/enc/games";
        fsType = "zfs";
      };

    swapDevices = [
      {
        device = "/dev/disk/by-partuuid/98edab3d-4f29-7742-b9e4-67393585a031";
        randomEncryption = {
          enable = true;
        };
      }
    ];
    systemd.tmpfiles.rules = [
      "w /sys/module/zswap/parameters/compressor - - - - zstd"
      "w /sys/module/zswap/parameters/zpool - - - - z3fold"
      "w /sys/module/zswap/parameters/enabled - - - - 1"
    ];

    hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  };
}
