{ config, ... }:

let
  shellCommon = config.local.shell;
in {
  config = {
    programs.bash = {
      enable = true;

      bashrcExtra = ''
        # The login shell should drop into fish in interactive mode
        if [[ $- == *i* && "''${BASH##*/}" == "bashim" && -z "$BASH_EXECUTION_STRING" ]]; then
          exec -a "$0" "${config.programs.fish.package}/bin/fish"
        fi
      '';

      shellAliases = shellCommon.aliases // shellCommon.abbreviations // {
        # Make aliases work under sudo
        sudo = "sudo ";
      };
    };
  };
}
