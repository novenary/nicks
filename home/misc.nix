{ config, lib, pkgs, ... }:

{
  config = {
    home = {
      packages = [
        pkgs.fd
        pkgs.file
        pkgs.fzf
        pkgs.git
        pkgs.hyperfine
        pkgs.jq
        pkgs.man-pages
        pkgs.man-pages-posix
        pkgs.schedtool
        pkgs.tio
        pkgs.wget

        pkgs.atool
        pkgs.zip
        pkgs.unzip
        pkgs.p7zip
        pkgs.unrar
      ];

      sessionVariables = rec {
        LESS = "FRiM";
        # systemd is stupid and overrides LESS with insane defaults
        SYSTEMD_LESS = LESS;

        FZF_DEFAULT_COMMAND = "true";
        FZF_DEFAULT_OPTS = lib.escapeShellArgs [
          "--cycle"
          "--marker=*"
          "--color=16,bg+:-1,fg+:-1:reverse,hl+:reverse"
        ];
      };
    };

    programs.ripgrep = {
      enable = true;
      arguments = [
        "--smart-case"
      ];
    };

    xdg.userDirs = let
      home = config.home.homeDirectory;
    in {
      enable = true;
      createDirectories = true;

      desktop = "${home}";
      documents = "${home}/documents";
      download = "${home}/downloads";
      music = "${home}/music";
      pictures = "${home}/images";
      publicShare = "${home}/public";
      templates = "${home}/documents/templates";
      videos = "${home}/videos";
    };
  };
}
