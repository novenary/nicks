local char = "▎"
require("ibl").setup {
	indent = {
		char = char,
		tab_char = char,
	},
	scope = {
		show_start = false,
		show_end = false,
	},
}
