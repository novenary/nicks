{ nixosConfig, ... }:

{
  config = {
    services.librespot = {
      enable = true;
      settings = {
        disable-audio-cache = true;
        disable-discovery = true;
        enable-oauth = true;
        name = "${nixosConfig.networking.hostName}";
        backend = "pulseaudio";

        # Hands off
        initial-volume = 100;
        volume-ctrl = "fixed";

        bitrate = 320;
        enable-volume-normalisation = true;
        normalisation-method = "basic";
      };
    };
  };
}
