local cterm = {
	bg_0 = "NONE",
	bg_1 = 0,
	bg_2 = 8,
	dim_0 = 7,
	fg_0 = "NONE",
	fg_1 = 15,

	red = 1,
	green = 2,
	yellow = 3,
	blue = 4,
	magenta = 5,
	cyan = 6,
	orange = 3,
	violet = 5,

	br_red = 9,
	br_green = 10,
	br_yellow = 11,
	br_blue = 12,
	br_magenta = 13,
	br_cyan = 14,
	br_orange = 11,
	br_violet = 13,
}
local variants = {
	light = {
		bg_0 = "#ffffff",
		bg_1 = "#ebebeb",
		bg_2 = "#cdcdcd",
		dim_0 = "#878787",
		fg_0 = "#474747",
		fg_1 = "#282828",

		red = "#d6000c",
		green = "#1d9700",
		yellow = "#c49700",
		blue = "#0064e4",
		magenta = "#dd0f9d",
		cyan = "#00ad9c",
		orange = "#d04a00",
		violet = "#7f51d6",

		br_red = "#bf0000",
		br_green = "#008400",
		br_yellow = "#af8500",
		br_blue = "#0054cf",
		br_magenta = "#c7008b",
		br_cyan = "#009a8a",
		br_orange = "#ba3700",
		br_violet = "#6b40c3",
	},
	dark = {
		bg_0 = "#181818",
		bg_1 = "#252525",
		bg_2 = "#3b3b3b",
		dim_0 = "#777777",
		fg_0 = "#b9b9b9",
		fg_1 = "#dedede",

		red = "#ed4a46",
		green = "#70b433",
		yellow = "#dbb32d",
		blue = "#368aeb",
		magenta = "#eb6eb7",
		cyan = "#3fc5b7",
		orange = "#e67f43",
		violet = "#a580e2",

		br_red = "#ff5e56",
		br_green = "#83c746",
		br_yellow = "#efc541",
		br_blue = "#4f9cfe",
		br_magenta = "#ff81ca",
		br_cyan = "#56d8c9",
		br_orange = "#fa9153",
		br_violet = "#b891f5",
	},
}

local guicolors = variants[vim.opt.background:get()]
local function section(arg)
	local ret = arg
	local fg = arg.fg
	if fg ~= nil then
		ret.fg = guicolors[fg]
		ret.ctermfg = cterm[fg]
	end
	local bg = arg.bg
	if bg ~= nil then
		ret.bg = guicolors[bg]
		ret.ctermbg = cterm[bg]
	end
	return ret
end

return {
	normal = {
		a = section {fg = "bg_1", bg = "green", gui = "bold"},
		b = section {fg = "fg_0", bg = "bg_2"},
		c = section {fg = "dim_0", bg = "bg_1"},
	},
	insert = {
		a = section {fg = "bg_1", bg = "blue", gui = "bold"},
	},
	terminal = {
		a = section {fg = "bg_1", bg = "blue", gui = "bold"},
	},
	visual = {
		a = section {fg = "bg_1", bg = "violet", gui = "bold"},
	},
	replace = {
		a = section {fg = "bg_1", bg = "red", gui = "bold"},
	},
	inactive = {
		a = section {fg = "dim_0", bg = "bg_2"},
		b = section {fg = "dim_0", bg = "bg_2"},
		c = section {fg = "dim_0", bg = "bg_2"},
	},
}
