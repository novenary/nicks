{ config, pkgs, ... }:

{
  config = {
    services.dunst = {
      enable = true;
      settings = {
        global = {
          follow = "keyboard";

          width = 300;
          height = 300;
          origin = "bottom-right";
          offset = "20x20";

          notification-limit = 5;
          idle_threshold = 120;

          separator-height = 4;
          frame-width = 4;
          frame_color = "#878787";

          font = "IBM Plex Sans 13";

          icon_position = "off";

          dmenu = "false";
          browser = "xdg-open";

          layer = "top";

          mouse_left_click = "do_action, close_current";
          mouse_middle_click = "close_current";
          mouse_right_click = "none";
        };

        urgency_low = {
          background = "#ffffff";
          foreground = "#474747";
          highlight = "#47474780";
          timeout = 10;
        };

        urgency_normal = {
          background = "#ffffff";
          foreground = "#474747";
          highlight = "#0064e480";
          timeout = 10;
        };

        urgency_critical = {
          background = "#ffffff";
          foreground = "#474747";
          frame_color = "#d6000c";
          highlight = "#d6000c80";
          timeout = 0;
        };

        fullscreen_delay_everything.fullscreen = "pushback";

        firefox_screenshot = {
          appname = "Nightly";
          summary = "Shot Copied";
          skip_display = true;
          history_ignore = true;
        };
      };
    };

    wayland.windowManager.sway.config.keybindings = let
      cfg = config.wayland.windowManager.sway.config;
    in {
      "${cfg.modifier}+tab" = "exec dunstctl close";
      "${cfg.modifier}+Shift+tab" = "exec dunstctl history-pop";
      "${cfg.modifier}+Ctrl+tab" = "exec dunstctl close-all";
    };
  };
}
