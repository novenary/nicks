local config_modules = {
	"general",
	"keymap",

	"asterisk",
	"completion",
	"git",
	"indent-blankline",
	"leap",
	"lualine",
	"osc52",
	"telescope",
	"treesitter",
	"undotree",
	"window-picker",
}
for _, mod in ipairs(config_modules) do
	require("vim.site.config." .. mod)
end
