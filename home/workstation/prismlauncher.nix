{ config, lib, pkgs, ... }:

{
  config = let
    inherit (lib) attrNames map mapAttrsToList;
    jreFarm = config.xdg.dataFile."PrismLauncher/jre";
  in {
    xdg.dataFile."PrismLauncher/jre".source = pkgs.linkFarm "prismlauncher-jres" {
      inherit (pkgs) jdk21 jdk17 jdk8;
    };
    home.packages = [
      (pkgs.prismlauncher.override {
        jdks = map (n: "${config.home.homeDirectory}/${jreFarm.target}/${n}") (attrNames jreFarm.source.entries);
      })
    ];
  };
}
