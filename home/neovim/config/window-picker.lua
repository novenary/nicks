local window_picker = require("window-picker")
window_picker.setup {
	hint = "floating-big-letter",
	selection_chars = "tnseridhaogmfuwy", -- colemak-dh
	show_prompt = false,
}

-- Utility functions taken from winshift.nvim
-- https://github.com/sindrets/winshift.nvim/blob/37468ed6f385dfb50402368669766504c0e15583/lua/winshift/lib.lua
local split_utils = {}

local function switch_window()
	local target = window_picker.pick_window()
	if target then
		vim.api.nvim_set_current_win(target)
	end
end

local function swap_windows()
	local target = window_picker.pick_window()
	if target then
		local tree = split_utils.get_layout_tree()

		local cur = vim.api.nvim_get_current_win()
		cur = split_utils.find_leaf(tree, cur)
		target = split_utils.find_leaf(tree, target)

		split_utils.swap_leaves(cur, target)
		vim.api.nvim_set_current_win(cur.winid)
	end
end

vim.keymap.set("n", "<Leader>q", switch_window)
vim.keymap.set("n", "<Leader>Q", swap_windows)

function split_utils.get_layout_tree()
	local function recurse(parent)
		local node = { type = parent[1] }

		if node.type == "leaf" then
			node.winid = parent[2]
		else
			for i, child in ipairs(parent[2]) do
				node[#node + 1] = recurse(child)
				node[#node].index = i
				node[#node].parent = node
			end
		end

		return node
	end

	return recurse(vim.fn.winlayout())
end

function split_utils.find_leaf(tree, winid)
	local function recurse(node)
		if node.type == "leaf" and node.winid == winid then
			return node
		else
			for _, child in ipairs(node) do
				local target = recurse(child)
				if target then
					return target
				end
			end
		end
	end

	return recurse(tree)
end

function split_utils.swap_leaves(a, b)
	function make_temp_split(leaf)
		vim.cmd(
			string.format(
				"noautocmd keepjumps %dwindo belowright %s",
				vim.api.nvim_win_get_number(leaf.winid),
				leaf.parent.type == "col" and "vsp" or "sp"
			)
		)
		local winid = vim.api.nvim_get_current_win()
		local opt = { vertical = leaf.parent.type == "col", rightbelow = false }
		return winid, opt
	end

	temp_a, opt_a = make_temp_split(a)
	temp_b, opt_b = make_temp_split(b)
	vim.fn.win_splitmove(a.winid, temp_b, opt_b)
	vim.fn.win_splitmove(b.winid, temp_a, opt_a)
	vim.api.nvim_win_close(temp_a, true)
	vim.api.nvim_win_close(temp_b, true)
end
