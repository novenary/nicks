# Basic system configuration
{ config, pkgs, ... }:

{
  config = {
    nix = {
      settings = {
        show-trace = true;
        auto-optimise-store = true;
        keep-outputs = true;
        keep-derivations = true;
        auto-allocate-uids = true;
        temp-dir = "/var/tmp";
        experimental-features = [
          "auto-allocate-uids"
          "flakes"
          "nix-command"
        ];
      };
      gc = {
        automatic = true;
        dates = "*-*-01 04:15";
        options = "--delete-older-than 32d";
      };
    };

    # Get completions working
    programs.fish.enable = true;
    documentation.man.generateCaches = false;
    environment.shellAliases = {
      ls = null;
      ll = null;
      l = null;
    };

    services.openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
        PermitRootLogin = "prohibit-password";
      };
      banner = ''
        ${""}
          ██╗  ██╗██╗    ██╗ █████╗ ██╗  ██╗   ███████╗██╗██████╗
          ██║ ██╔╝██║    ██║██╔══██╗██║ ██╔╝   ╚══███╔╝██║██╔══██╗
          █████╔╝ ██║ █╗ ██║███████║█████╔╝      ███╔╝ ██║██████╔╝
          ██╔═██╗ ██║███╗██║██╔══██║██╔═██╗     ███╔╝  ██║██╔═══╝
          ██║  ██╗╚███╔███╔╝██║  ██║██║  ██╗██╗███████╗██║██║
          ╚═╝  ╚═╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚══════╝╚═╝╚═╝

      '';
    };

    services.journald.extraConfig = ''
      SystemMaxUse=100M
      MaxFileSec=7day
    '';

    boot.kernel.sysctl = {
      "vm.vfs_cache_pressure" = 50;
      "vm.dirty_background_bytes" = 16777216;
      "vm.dirty_bytes" = 50331648;
      "kernel.dmesg_restrict" = false;
    };

    services.dbus.implementation = "broker";

    environment.systemPackages = [
      pkgs.pciutils
      pkgs.usbutils
    ];

    boot.tmp.useTmpfs = true;
  };
}
