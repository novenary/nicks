#!/usr/bin/env fish

set -l command (find -L $PATH/ \
	-mindepth 1 -maxdepth 1 \
	-type f -executable \
	-printf '%f\0' 2>/dev/null \
	| sort -zu \
	| fzf --read0 --prompt='run> ')
and swaymsg "exec '$command'"
