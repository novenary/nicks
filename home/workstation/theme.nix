{ config, lib, pkgs, ... }:

let
  kvantumTheme = "KvGnome";
  gtkCfg = config.gtk;

  qt5ctParams = {
    fontSpec = name: size: weight: "${name},${toString size},-1,5,50,0,0,0,0,0,${weight}";
  };
  qt6ctParams = {
    fontSpec = name: size: weight: "${name},${toString size},-1,5,400,0,0,0,0,0,0,0,0,0,0,1,${weight}";
  };
  qtctConfig = params: ''
    [Appearance]
    icon_theme=${gtkCfg.iconTheme.name}
    standard_dialogs=gtk3
    style=kvantum

    [Fonts]
    general="${params.fontSpec gtkCfg.font.name gtkCfg.font.size "Regular"}"
    fixed="${params.fontSpec "IBM Plex Mono" gtkCfg.font.size "Regular"}"
  '';
in {
  config = {
    gtk = {
      enable = true;
      font = {
        package = pkgs.ibm-plex;
        name = "IBM Plex Sans";
        size = 10;
      };
      iconTheme = {
        package = pkgs.adwaita-icon-theme;
        name = "Adwaita";
      };
      theme = {
        package = pkgs.gnome-themes-extra;
        name = "Adwaita";
      };
    };

    qt = {
      enable = true;
      platformTheme.name = "qtct";
    };

    home.packages = [
      pkgs.libsForQt5.qtstyleplugin-kvantum
      pkgs.qt6Packages.qtstyleplugin-kvantum
    ];

    xdg.configFile = {
      "Kvantum/kvantum.kvconfig".text = ''
        [General]
        theme=${kvantumTheme}#
      '';
      "Kvantum/${kvantumTheme}#/${kvantumTheme}#.kvconfig".source =
        pkgs.concatText "${kvantumTheme}#.kvconfig" [
          "${pkgs.libsForQt5.qtstyleplugin-kvantum}/share/Kvantum/${kvantumTheme}/${kvantumTheme}.kvconfig"
          (builtins.toFile "override.kvconfig" ''
            [%General]
            shadowless_popup=true
          '')
        ];
        "qt5ct/qt5ct.conf".text = qtctConfig qt5ctParams;
        "qt6ct/qt6ct.conf".text = qtctConfig qt6ctParams;
    };
  };
}
