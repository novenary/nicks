{ config, pkgs, ... }:

{
  config = {
    # TODO: add a nice way to configure this to nixos
    environment.etc."u2f_keys" = let
      systemOwner = config.users.users.systemOwner;
    in {
      mode = "0444";
      source = pkgs.writeText "u2f_keys" ''
        ${systemOwner.name}:XYlgNclWXMS1oYwI/tPSsp5xNzH1Hx9gWW131kPjhnIvNj5Oey6RU88G1rPc5KF/dUQnOhHI/daeqbo7hp/TNvslfn31yfO2u/tVuGxJZG5kVyea5BYUxp1FmxrOiLhkiYCwhHgO9jyTxyksC0qoqmCmReRirk123haURu7WKyU=,ptkLPRiyRP9slcvda7X0klcP8fkR03Ym3GlnbHxg2g8=,eddsa,+pin:/ssBACJrbSz4tmnoOFkJmZOW666Jiuzz2PBkCECn0/OQ0AHy2I3lftZGVhptrEmhuIm0XAWqLvS3OEIQArJKW8CMYfMVyhlIf6tnVJX8xa7NkkGdEOKzFNxkKy1CBQyqO0FPsyTFT2BrRLD5HLBR3V/Z/TxUT7wayY3cQ2dGNkM=,SZflsHFzQpd1YQJcDr0+tGmqLbmdjiNvYV3oWXWu+P8=,eddsa,+pin:snjUcAH7qdPYz+0JHBKs0VDMs51rafRS3fvijRX3cIscK3fK8TJYXzwj0HJtJkmJuhVsvdat0WLDh+yfNc30wVWJKr426xwhhIknbF7mfDJQq0qDhMINp07aFvDA+26J8vOSw5js5oUDhNXt0ca0R+oTblxzpdKc6R5hOpiuw5A=,/kbAbzoepWsMo5d31e4Uo3JxFJuVsWw/MsTnWkyZN4w=,eddsa,+pin
      '';
    };

    security.pam.u2f = {
      enable = true;
      settings = {
        origin = "pam://enneanet";
        authfile = "/etc/u2f_keys";
        authpending_file = "";
        userpresence = 0;
        pinverification = 1;
      };
    };
  };
}
