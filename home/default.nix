{ lib, nixosConfig, ... }:

{
  imports = import ./module-list.nix;

  options = {
    miscAttrs = lib.mkOption {
      type = lib.types.attrs;
      description = "Settings shared by various parts of the configuration.";
    };
  };

  config = {
    home.stateVersion = "24.11";

    nix.gc = let
      sysConfig = nixosConfig.nix.gc;
    in (builtins.removeAttrs sysConfig [ "dates" ]) // {
      frequency = sysConfig.dates;
    };
  };
}
