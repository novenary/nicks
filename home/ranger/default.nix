{ pkgs, ... }:

{
  config = {
    home.packages = [
      pkgs.ranger
    ];

    xdg.configFile = {
      "ranger/rc.conf".text = ''
        map <escape> move left=1
        map <c-p> move up=1
        map <c-n> move down=1
        map <delete> console delete

        set preview_images true
        set preview_images_method sixel

        set colorscheme=walrus

        set hidden_filter=^\.
      '';
      "ranger/rifle.conf".text = builtins.readFile ./rifle.conf;
      "ranger/colorschemes/walrus.py".text = builtins.readFile ./colors.py;
    };
  };
}
