final: prev: {
  sway-unwrapped = prev.sway-unwrapped.overrideAttrs (old: {
    patches = old.patches or [] ++ [
      #  Don't send wl_pointer_leave when hide_cursor activates
      (final.fetchpatch {
        url = "https://github.com/9ary/sway9/commit/993cb218006524eb55b5b7a37fc2d046ba1b7c8f.patch";
        hash = "sha256-hsBOziCSAjE8nDhLtxJeIbCA+YI2eEaHwcotkrWUjTM=";
      })
    ];
  });
}
