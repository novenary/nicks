{ flakeInputs, pkgs', ... }:

let
  pkgsPatched = pkgs'.applyPatches {
    name = "nixpkgs-patched";
    src = flakeInputs.nixpkgs;
    patches = [
    ];
  };

  patchedModules = [
  ];
in {
  disabledModules = patchedModules;
  imports = map (m: "${pkgsPatched}/nixos/modules/${m}") patchedModules;
}
