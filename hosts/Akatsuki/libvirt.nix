{ config, pkgs, ... }:

{
  config = let
    systemOwner = config.users.users.systemOwner;
  in {
    virtualisation.libvirtd = {
      enable = true;
      qemu.swtpm.enable = true;
      qemu.ovmf.enable = true;
      qemu.verbatimConfig = ''
        cgroup_device_acl = [
            "/dev/null", "/dev/full", "/dev/zero",
            "/dev/random", "/dev/urandom",
            "/dev/ptmx", "/dev/kvm", "/dev/kvmfr0"
        ]
      '';
    };
    programs.dconf.enable = true;
    environment.systemPackages = [ pkgs.virt-manager ];

    boot.extraModulePackages = [ config.boot.kernelPackages.kvmfr ];
    boot.kernelModules = [ "vfio-pci" "kvmfr" ];
    boot.extraModprobeConfig = ''
      options vfio-pci ids=1002:699f,1002:aae0
      softdep amdgpu pre: vfio-pci
      options kvmfr static_size_mb=128
    '';
    services.udev.packages = [
      (pkgs.writeTextFile {
        name = "kvmfr-udev-rules";
        text = ''
          SUBSYSTEM=="kvmfr", GROUP="kvm", MODE="0660", TAG+="uaccess"
        '';
        destination = "/etc/udev/rules.d/70-kvmfr.rules";
      })
    ];
    users.users.systemOwner.extraGroups = [ "libvirtd" ];

    services.samba = {
      enable = true;
      openFirewall = true;

      settings = {
        global = {
          "server min protocol" = "SMB3_11";
          "unix extensions" = "no";
          "server smb encrypt" = "off";

          # Performance
          "use sendfile" = "yes";
          "min receivefile size" = 16384;
          "aio read size" = 1;
          "aio write size" = 1;
          "socket options" = "IPTOS_LOWDELAY TCP_NODELAY IPTOS_THROUGHPUT SO_RCVBUF=131072 SO_SNDBUF=131072";
        };

        winshare = {
          "wide links" = "yes";
          "acl allow execute always" = "yes";
          path = "${systemOwner.home}/winshare";
          writable = "yes";
        };
      };
    };
  };
}
