{ buildPythonApplication
, flit-core
, psutil
, pulsectl-asyncio
, soco
, aiohttp
}:

buildPythonApplication {
  name = "swaystatus";

  src = ./.;

  format = "pyproject";

  build-system = [
    flit-core
  ];

  dependencies = [
    psutil
    pulsectl-asyncio
    soco
    aiohttp
  ];
}
