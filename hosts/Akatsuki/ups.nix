{ ... }:

{
  config = {
    power.ups = {
      enable = true;

      users = {
        upsmon-main = {
          # sigh...
          # sudo touch /etc/nut/upsmon-main.password
          # sudo chmod 400 /etc/nut/upsmon-main.password
          # sudo sh -c 'spwgen strong > /etc/nut/upsmon-main.password'
          passwordFile = "/etc/nut/upsmon-main.password";
          upsmon = "primary";
        };
      };

      upsmon = {
        monitor.main = {
          user = "upsmon-main";
        };
      };

      ups.main = {
        driver = "nutdrv_qx";
        port = "auto";
        directives = [
          "vendorid = 0001"
          "productid = 0000"
          "product = MEC0003"
          "vendor = MEC"
        ];
      };
    };
  };
}
