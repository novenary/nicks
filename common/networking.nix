{ ... }:

{
  config = {
    networking.useDHCP = false;
    networking.useNetworkd = true;
    services.nscd.enableNsncd = true;

    systemd.network.networks."50-wired" = {
      matchConfig = {
        Type = "ether";
        Name = "en*";
      };
      networkConfig = {
        DHCP = "yes";
        IPv6PrivacyExtensions = true;
        IPv6LinkLocalAddressGenerationMode = "stable-privacy";
      };
      ipv6AcceptRAConfig = {
        Token = "prefixstable";
      };
    };

    services.resolved = {
      enable = true;
      dnssec = "false";
      fallbackDns = [];
    };

    services.tailscale = {
      enable = true;
    };
  };
}
