[
  ./hosts

  ./colors.nix
  ./direnv.nix
  ./eza.nix
  ./htop.nix
  ./misc.nix
  ./neovim
  ./ranger
  ./ssh.nix

  ./shell/common.nix
  ./shell/dircolors.nix
  ./shell/bash.nix
  ./shell/fish.nix

  ./workstation
]
