{ config, lib, pkgs, ... }:

{
  config = lib.mkIf config.systemProfile.isWorkstation {
    programs.nix-ld.enable = true;

    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      jack.enable = true;
    };

    fonts = {
      fontconfig.subpixel = {
        rgba = "none";
        lcdfilter = "none";
      };

      enableDefaultPackages = false;
      packages = [
        pkgs.dejavu_fonts
        pkgs.gyre-fonts # substitutes for standard PostScript fonts
        pkgs.liberation_ttf # substitutes for some Microsoft fonts
        pkgs.liberation-sans-narrow

        pkgs.noto-fonts
        pkgs.noto-fonts-cjk-sans
        pkgs.noto-fonts-cjk-serif
        pkgs.noto-fonts-color-emoji
      ];
    };

    programs.sway = {
      enable = true;
      extraPackages = [];
    };

    xdg.portal.wlr = {
      enable = true;
      settings = {
        screencast = {
          chooser_type = "none";
        };
      };
    };

    services.yubikey-agent.enable = true;
    systemd.user.services.yubikey-agent = {
      wantedBy = lib.mkForce [ "graphical-session.target" ];
      partOf = [ "graphical-session.target" ];
    };
    programs.gnupg.agent.pinentryPackage = pkgs.pinentry-qt;

    services.greetd = {
      enable = true;
      settings = {
        default_session = {
          command = let
            sway = "${pkgs.sway}/bin/sway";
            footCfg = (pkgs.formats.ini {}).generate "foot_greetd.ini" {
              main = {
                font = "monospace:size=14";
                dpi-aware = "yes";
              };

              # Selenized black
              colors = {
                background = "181818";
                foreground = "b9b9b9";

                regular0 = "252525";
                regular1 = "ed4a46";
                regular2 = "70b433";
                regular3 = "dbb32d";
                regular4 = "368aeb";
                regular5 = "eb6eb7";
                regular6 = "3fc5b7";
                regular7 = "777777";

                bright0 = "3b3b3b";
                bright1 = "ff5e56";
                bright2 = "83c746";
                bright3 = "efc541";
                bright4 = "4f9cfe";
                bright5 = "ff81ca";
                bright6 = "56d8c9";
                bright7 = "dedede";
              };
              cursor.color = "181818 56d8c9";
            };
            foot = "${pkgs.foot}/bin/foot -c ${footCfg}";
            tuigreet = lib.escapeShellArgs [
              "${pkgs.greetd.tuigreet}/bin/tuigreet"
              "--user-menu"
              "-c" "~/.session"
              "-t"
              "-g" "I lost the game ._."
              "--asterisks"
              "--window-padding" 1
            ];
            swayCfg = pkgs.writeText "greetd_sway" ''
              exec "${foot} -- ${tuigreet}; swaymsg exit"
              default_border none
              seat * hide_cursor 1
              input "type:keyboard" {
                xkb_layout us(colemak_dh_ortho)
                xkb_options lv3:ralt_alt
              }
            '';
          in "${sway} -c ${swayCfg}";
        };
      };
    };

    hardware.acpilight.enable = true;

    hardware.bluetooth.enable = true;

    programs.steam.enable = true;

    services.udev.packages = [
      pkgs.android-udev-rules
      # TODO upstream this to nixpkgs
      (pkgs.runCommand "openocd-udev-rules" {} (let
        rulesDir = "etc/udev/rules.d";
        rulesPath = "${rulesDir}/60-openocd.rules";
      in ''
        mkdir -p "$out/${rulesDir}"
        substitute "${pkgs.openocd}/${rulesPath}" "$out/${rulesPath}" \
          --replace 'MODE="660", ' "" \
          --replace 'GROUP="plugdev", ' ""
      ''))
    ];

    services.udisks2 = {
      enable = true;
      mountOnMedia = true;
    };

    programs.wireshark = {
      enable = true;
      package = pkgs.wireshark-qt;
    };
    users.users.systemOwner.extraGroups = [
      "video" # for acpilight
      "wireshark"
    ];

    programs.rqoob.enable = true;

    services.speechd.enable = false;
  };
}
