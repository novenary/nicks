let
  pkgs = import <nixpkgs> {};
  pythonPkgs = pkgs.python3.pkgs;
  swaystatus = pythonPkgs.callPackage ./default.nix {};
in
  pkgs.mkShell {
    name = "swaystatus";
    inputsFrom = [swaystatus];
    packages = [
      pythonPkgs.python-lsp-server
    ];
  }
