# Based on https://github.com/PatrickF1/fzf.fish
set -l fd_args --color=always
set -l fzf_args --multi --ansi $fzf_args

set -l token (commandline -t)
# expand any variables or leading tilde (~) in the token
# unescape because it's already quoted so backslashes will mess up the path
set -l expanded_token (eval echo -- $token | string unescape)

set -l selection_prefix ''
if string match -q -- "*/" $expanded_token && test -d "$expanded_token"
	# If the current token is a directory and has a trailing slash,
	# then use it as fd's base directory.
	set -a fd_args --base-directory=$expanded_token
	set -p fzf_args --prompt="$expanded_token> "
	set selection_prefix $expanded_token
else
	set -p fzf_args --query="$expanded_token"
end

set -l selection $selection_prefix(fd $fd_args 2>/dev/null | fzf $fzf_args)
and commandline -tr -- (string escape -n -- $selection | string join ' ')

commandline -f repaint
