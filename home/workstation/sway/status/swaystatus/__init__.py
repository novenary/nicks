#!/usr/bin/env python3

import asyncio
import glob
import json
import os
import platform
from queue import Empty
import sys
import time

import psutil
import pulsectl_asyncio
import soco
from soco import events_asyncio as soco_aio

from . import mpd

HOST = platform.node()
INTERVAL = 1
COLOR_GOOD = "#1D9700"
COLOR_DEGRADED = "#C49700"
COLOR_BAD = "#D6000C"
BACKGROUND = "#CDCDCD"
BLOCK_MARGIN = 4
SEPARATOR_WIDTH = 6

soco.config.EVENTS_MODULE = soco_aio


class CpuLoad:
    def render(self):
        p = int(psutil.cpu_percent())
        return {"full_text": f"\uF2DB {p:02}%"}


class Temperature:
    def __init__(self, label, path):
        self.label = label
        self.file = open(glob.glob(path)[0])

    def render(self):
        self.file.seek(0, 0)
        t = int(self.file.read().strip()) // 1000
        return {
                "instance": self.label,
                "full_text": f"{self.label} {t:02}°C"}


class Time:
    def __init__(self, format):
        self.format = format

    def render(self):
        return {"full_text": time.strftime(self.format)}


class Battery:
    BATTERY_STATUSES = {
            "Charging": "\uF0E7",
            "Full": "\uF240",
            "Discharging": "\uF242"}

    def __init__(self, path):
        self.file = open(path)

    def render(self):
        charge = None
        charge_rel = None
        charge_full = None
        charge_full_design = None
        watts = None
        discharge_rate = None
        voltage = None
        status = "?"

        self.file.seek(0, 0)
        for line in self.file.readlines():
            k, v = line.rstrip("\n").split("=", 1)
            k = k.replace("POWER_SUPPLY_", "")
            if k in ("ENERGY_NOW", "CHARGE_NOW"):
                charge = int(v)
            elif k.endswith("_FULL"):
                charge_full = int(v)
            elif k.endswith("_FULL_DESIGN"):
                charge_full_design = int(v)
            elif k == "CAPACITY":
                charge_rel = int(v) / 100
            elif k in ("POWER_NOW", "CURRENT_NOW"):
                watts = k.startswith("POWER")
                discharge_rate = int(v) / 1e6
            elif k == "VOLTAGE_NOW":
                voltage = int(v) / 1e3
            elif k == "STATUS":
                status = self.BATTERY_STATUSES.get(v, "\uF059")

        full = charge_full_design or charge_full
        if charge is not None and full is not None:
            charge_rel = charge / full

        if not watts and discharge_rate is not None and voltage is not None:
            discharge_rate *= voltage

        return {
                "full_text":
                        f"{status} {charge_rel:.0%} {discharge_rate:.2f}W",
                "color": COLOR_BAD if charge_rel <= .2 else None}


class PAVolume:
    def __init__(self):
        self.event_loop = None
        self.event = asyncio.Event()
        self.volume = None

    async def _event_loop_monitor(self):
        while True:
            pulse = pulsectl_asyncio.PulseAsync("swaystatus")
            await pulse.connect(wait=True)

            try:
                await self._event_loop(pulse)
            except:
                self.volume = None
                await asyncio.sleep(1)
                pass
            finally:
                pulse.disconnect()

    async def _event_loop(self, pulse):
        async def update_status():
            server_info = await pulse.server_info()
            default_sink_name = server_info.default_sink_name
            sink_info = await pulse.get_sink_by_name(default_sink_name)

            self.volume = sink_info.volume.value_flat
            self.mute = bool(sink_info.mute)
            self.event.set()

        await update_status()
        async for event in pulse.subscribe_events("sink"):
            await update_status()

    async def update(self):
        if self.event_loop is None:
            self.event_loop = asyncio.create_task(self._event_loop_monitor())

        await self.event.wait()
        self.event.clear()
        return self

    def render(self):
        if self.volume is not None:
            if not self.mute:
                return {"full_text": f"\uF028 {self.volume:.0%}"}
            else:
                return {"full_text": f"\uF6A9"}
        else:
            return {"full_text": "\uF071", "color": COLOR_BAD}


class Mpd:
    def __init__(self):
        self.mpd = mpd.MpdClient()
        self.first_update = True
        self.state = "stop"

    async def update(self):
        while True:
            try:
                if not self.first_update:
                    await self.mpd.idle("player")
                self.first_update = False
                status, song = await asyncio.gather(
                        self.mpd.status(),
                        self.mpd.currentsong())
                self.state = status.get("state")
                self.title = song.get("title")
                self.artist = song.get("artist")
                self.album = song.get("album")
                return self
            except ConnectionError:
                self.__init__()

    def render(self):
        if self.state != "stop":
            icon = "\uF04B" if self.state == "play" else "\uF04C"
            full = f"{icon} {self.artist} – {self.title} ({self.album})"
        else:
            full = "\uF04D"
        short = f"{full[:160]}..."
        return {"full_text": full, "short_text": short}


class SonosVolume:
    VOLUME_STEP = 5

    def __init__(self, ip):
        self.event = asyncio.Event()
        self.volume = None
        self.ip = ip
        self.device = None
        self.sub = None
        self.volume_server = None
        self.odd_volume = False

    async def _connect(self):
        if self.sub is not None:
            await self.sub.unsubscribe()
            await soco_aio.event_listener.async_stop()
        while True:
            try:
                self.device = soco.SoCo(self.ip)
                self.sub = await self.device.renderingControl.subscribe(
                    requested_timeout=30,
                    auto_renew=True,
                )
                self.sub.callback = self._on_event
                self.sub.auto_renew_fail = self._auto_renew_fail
                break
            except:
                await asyncio.sleep(1)

        if self.volume_server is None:
            self.volume_server = await asyncio.start_unix_server(
                    self._volume_client_connnected,
                    path=f"/run/user/{os.getuid()}/sonos_volume")

    async def _volume_client_connnected(self, r, w):
        data = await r.read()
        for byte in data:
            if self.volume is None:
                break
            correction = 0
            if self.odd_volume:
                error = self.volume % self.VOLUME_STEP
                if error < self.VOLUME_STEP / 2:
                    correction = -error
                else:
                    correction = self.VOLUME_STEP - error
            if byte == ord("+"):
                self.device.set_relative_volume(self.VOLUME_STEP + correction)
                self.odd_volume = False
            elif byte == ord("-"):
                self.device.set_relative_volume(-self.VOLUME_STEP + correction)
                self.odd_volume = False
            elif byte == ord("x"):
                self.device.mute = not self.mute
            elif byte == ord("l"):
                self.device.switch_to_line_in()
                self.device.play()
        w.write_eof()

    def _on_event(self, event):
        volume = event.variables.get("volume")
        if volume is not None:
            self.volume = int(volume["Master"])
            self.odd_volume = self.volume % self.VOLUME_STEP != 0
        mute = event.variables.get("mute")
        if mute is not None:
            self.mute = bool(int(mute["Master"]))
        self.event.set()

    def _auto_renew_fail(self, exc):
        self.volume = None
        self._reconnect_task = asyncio.create_task(self._connect())

    async def update(self):
        if self.sub is None:
            await self._connect()

        await self.event.wait()
        self.event.clear()

        return self

    def render(self):
        if self.volume is not None:
            if not self.mute:
                return {"full_text": f"\uF028 {self.volume}%"}
            else:
                return {"full_text": f"\uF6A9"}
        else:
            return {"full_text": "\uF071", "color": COLOR_BAD}


blocks = []

blocks.append(PAVolume())

if HOST == "Akatsuki":
    blocks.append(SonosVolume("10.0.0.138"))
    blocks.append(Mpd())

blocks.append(CpuLoad())
if HOST == "Akatsuki":
    blocks.append(Temperature(
            "\uF2DB",
            "/sys/bus/pci/drivers/k10temp/0000:*/hwmon/hwmon*/temp1_input"))
    blocks.append(Temperature(
            "\uF1B2",
            "/sys/bus/pci/drivers/amdgpu/0000:*/hwmon/hwmon*/temp1_input"))
else:
    blocks.append(Temperature(
            "\uF2DB",
            "/sys/devices/platform/coretemp.0/hwmon/hwmon*/temp1_input"))

if HOST == "Hitagi":
    blocks.append(Battery("/sys/class/power_supply/BAT1/uevent"))

blocks.append(Time("\uF783 %a %d %b %H:%M"))


def print_unbuffered(*lines):
    for line in lines:
        sys.stdout.write(line + "\n")
    sys.stdout.flush()


async def main():
    # Queue updates for all async blocks
    updates = []
    for block in blocks:
        try:
            updates.append(asyncio.create_task(block.update()))
        except AttributeError:
            pass

    header = {
            "version": 1,
            "stop_signal": 0,
            "cont_signal": 0}
    print_unbuffered(json.dumps(header, ensure_ascii=False), "[")

    while True:
        render = []
        for block in blocks:
            r = block.render()
            if r is not None:
                render.append({
                    "name": block.__class__.__name__,
                    "background": BACKGROUND,
                    "border": BACKGROUND,
                    "border_left": BLOCK_MARGIN,
                    "border_right": BLOCK_MARGIN,
                    "separator_block_width": SEPARATOR_WIDTH,
                    "separator": False,
                    **r,
                })
        render[-1]["separator_block_width"] = 0
        print_unbuffered(json.dumps(render, ensure_ascii=False) + ",")

        if not updates:
            await asyncio.sleep(INTERVAL)
            continue
        done, pending = await asyncio.wait(
                updates, timeout=INTERVAL, return_when=asyncio.FIRST_COMPLETED)
        for update in done:
            # Updates return the block they belong to so it's easier to queue
            # a new update again
            updates.append(asyncio.create_task(update.result().update()))
            updates.remove(update)


def run():
    asyncio.run(main())

if __name__ == "__main__":
    run()
