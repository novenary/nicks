{ flakeInputs, pkgs, ... }:

let
  pkgsOverlaid = (import "${flakeInputs.mozilla}/firefox-overlay.nix") pkgs pkgs;
  fxLib = pkgsOverlaid.lib.firefoxOverlay;

  metadata = builtins.fromJSON (builtins.readFile "${flakeInputs.firefox-nightly}/latest.json");

  firefoxPkgs =
    builtins.mapAttrs
    (
      n: v: let
        cv = v;
        cvi = metadata.${pkgs.system}."versionInfo".${n};
      in
        fxLib.firefoxVersion (cv // {info = cvi;})
    )
    metadata.${pkgs.system}.variants;
in {
  config = {
    home = {
      packages = [
        (let
          fx = firefoxPkgs.firefox-nightly-bin;
          fxPatched = fx.unwrapped.overrideAttrs (old: {
            postPatch = old.postPatch or "" + ''
              sed -i omni.ja \
                -e \
                's/!!Services.prefs.getIntPref("browser.display.use_document_fonts")/true                                                             /'
            '';
          });
        in pkgs.wrapFirefox fxPatched {
          pname = "${fxPatched.binaryName}-bin";
          desktopName = "Firefox Nightly";
          wmClass = "firefox-nightly";
        })
      ];

      sessionVariables = {
        BROWSER = "firefox-nightly";
      };
    };
  };
}
