#!/usr/bin/env bash

set -e

stripe() {
	for color in "$@"; do
		printf '\x1b[48;5;%sm   ' "$color"
	done
	printf '\x1b[0m\n'
}

stripe {0..7} 16 17
stripe {0..7} 16 17
stripe {8..15} 18 19
stripe {8..15} 18 19
