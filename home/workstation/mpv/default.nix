{ pkgs, ... }:

{
  config = {
    programs.mpv = {
      enable = true;

      defaultProfiles = [ "gpu-hq" ];
      config = {
        vo = "gpu-next";
        hwdec = true;

        interpolation = true;
        tscale = "oversample";
        video-sync = "display-resample";

        fullscreen = true;
        osc = false;
        audio-display = false;
        hr-seek = true;
        keep-open = true;

        screenshot-template = "zcreencap%n";

        alang = "jpn,eng,fra";
        slang = "eng,fra";

        use-filedir-conf = true;

        ytdl-format = "bestvideo[height<=?2160]+bestaudio/best";
        ytdl-raw-options = "sub-langs=all,write-subs=,write-auto-subs=";
      };

      bindings = {
        WHEEL_UP = "seek 5";
        WHEEL_DOWN = "seek -5";
      };
    };

    home.packages = [
      pkgs.open-in-mpv
    ];
  };
}
